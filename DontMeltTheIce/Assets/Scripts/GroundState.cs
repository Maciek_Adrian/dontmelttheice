﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundState : MonoBehaviour
{
    [SerializeField] private GroundType GroundType;

    [SerializeField] private GameObject LeftNeighbour;
    [SerializeField] private GameObject RightNeighbour;
    [SerializeField] private GameObject FrontNeighbour;
    [SerializeField] private GameObject BackNeighbour;

    [SerializeField] private int GroundLevel;
    
    public int GetGroundLevel()
    {
        return GroundLevel;
    }

    public GameObject GetLeftNeighbour()
    {
        return LeftNeighbour;
    }

    public GameObject GetRightNeighbour()
    {
        return RightNeighbour;
    }

    public GameObject GetFrontNeighbour()
    {
        return FrontNeighbour;
    }

    public GameObject GetBackNeighbour()
    {
        return BackNeighbour;
    }

    public GroundType GetGroundType()
    {
        return GroundType;
    }

}
