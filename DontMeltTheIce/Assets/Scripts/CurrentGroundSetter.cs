﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentGroundSetter : MonoBehaviour
{
    [SerializeField] private GroundState GroundState;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Something entered" + other.tag);
        if (!other.CompareTag("Player")) return;

        other.GetComponent<PlayerController>().SetCurrentGround(GroundState);
    }
}
