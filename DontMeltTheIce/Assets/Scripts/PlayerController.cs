﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private bool input = true;

    [SerializeField] private Transform Player;
    
    [SerializeField] private Transform Forward;
    [SerializeField] private Transform Back;
    [SerializeField] private Transform Left;
    [SerializeField] private Transform Right;

    [SerializeField] private Transform ForwardTop;
    [SerializeField] private Transform BackTop;
    [SerializeField] private Transform LeftTop;
    [SerializeField] private Transform RightTop;

    [SerializeField] private Transform Center;

    private int step = 9;
    private float speed = 0.01f;

    [SerializeField] private GroundState CurrentGround;
    [SerializeField] private Direction Direction;

    [SerializeField] private Transform Arrow;
    [SerializeField] private Transform ArrowCenter;

    [SerializeField] private FireTrigger FireTrigger;



    // Update is called once per frame
    void Update()
    {
        if (input)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                if (FireTrigger.CheckIfIceMelting()) return;
                switch (Direction)
                {
                    case Direction.FORWARD:
                        StepForward();
                        break;
                    case Direction.BACK:
                        StepBack();
                        break;
                    case Direction.LEFT:
                        StepLeft();
                        break;
                    case Direction.RIGHT:
                        StepRight();
                        break;

                }
            }
            else if (Input.GetKeyDown(KeyCode.C))
            {
                if (FireTrigger.CheckIfIceMelting()) return;
                input = false;
                StartCoroutine("RotateArrow");
                Direction = (Direction)(((int)Direction+1)%4);
                Debug.Log(Direction);
            }

        }
        
    }

    public void StepForward()
    {
        if (CurrentGround.GetFrontNeighbour() == null) return;
        if (!CurrentGround.GetFrontNeighbour().gameObject.activeInHierarchy) return;


        GroundState frontNeighbourState = CurrentGround.GetFrontNeighbour().GetComponent<GroundState>();
        if (frontNeighbourState.GetGroundLevel() == CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoForward");
        }
        else if(frontNeighbourState.GetGroundLevel() == 1 + CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoForwardTop");
        }
        else if(frontNeighbourState.GetGroundLevel() == (-1) + CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoForwardDown");
        }
    }

    public void StepBack()
    {
        if (CurrentGround.GetBackNeighbour() == null) return;
        if (!CurrentGround.GetBackNeighbour().gameObject.activeInHierarchy) return;


        GroundState frontNeighbourState = CurrentGround.GetBackNeighbour().GetComponent<GroundState>();
        if (frontNeighbourState.GetGroundLevel() == CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoBack");
        }
        else if (frontNeighbourState.GetGroundLevel() == 1 + CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoBackTop");
        }
        else if (frontNeighbourState.GetGroundLevel() == (-1) + CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoBackDown");
        }
    }

    public void StepLeft()
    {
        if (CurrentGround.GetLeftNeighbour() == null) return;
        if (!CurrentGround.GetLeftNeighbour().gameObject.activeInHierarchy) return;


        GroundState frontNeighbourState = CurrentGround.GetLeftNeighbour().GetComponent<GroundState>();
        if (frontNeighbourState.GetGroundLevel() == CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoLeft");
        }
        else if (frontNeighbourState.GetGroundLevel() == 1 + CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoLeftTop");
        }
        else if (frontNeighbourState.GetGroundLevel() == (-1) + CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoLeftDown");
        }
    }

    public void StepRight()
    {
        if (CurrentGround.GetRightNeighbour() == null) return;
        if (!CurrentGround.GetRightNeighbour().gameObject.activeInHierarchy) return;

        GroundState frontNeighbourState = CurrentGround.GetRightNeighbour().GetComponent<GroundState>();
        if (frontNeighbourState.GetGroundLevel() == CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoRight");
        }
        else if (frontNeighbourState.GetGroundLevel() == 1 + CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoRightTop");
        }
        else if (frontNeighbourState.GetGroundLevel() == (-1) + CurrentGround.GetGroundLevel())
        {
            input = false;
            StartCoroutine("GoRightDown");
        }
    }

    public IEnumerator RotateArrow()
    {
        for(int i =0; i< (90/step); i++)
        {
            Arrow.RotateAround(ArrowCenter.position, -ArrowCenter.up, step);
            yield return new WaitForSeconds(speed);
        }
        input = true;
        yield return null;
    }
    public IEnumerator GoForward()
    {
        for(int i = 0; i< (90/step); i++)
        {
            Player.RotateAround(Forward.position, Center.right, step);
            ArrowCenter.position += new Vector3(0, 0, 2f)/(90f/(float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(0,0,2);
        input = true;
        yield return null;
    }

    public IEnumerator GoBack()
    {
        for (int i = 0; i < (90 / step); i++)
        {
            Player.RotateAround(Back.position, -Center.right, step);
            ArrowCenter.position += new Vector3(0, 0, -2f) / (90f / (float)step);
            yield return new WaitForSeconds(speed);
        }
        Center.position += new Vector3(0, 0, -2);
        input = true;
        yield return null;
    }

    public IEnumerator GoLeft()
    {
        for (int i = 0; i < (90 / step); i++)
        {
            Player.RotateAround(Left.position, Center.forward, step);
            ArrowCenter.position += new Vector3(-2f, 0, 0) / (90f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(-2, 0, 0);
        input = true;
        yield return null;
    }

    public IEnumerator GoRight()
    {
        for (int i = 0; i < (90 / step); i++)
        {
            Player.RotateAround(Right.position, -Center.forward, step);
            ArrowCenter.position += new Vector3(2f, 0, 0) / (90f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(2, 0, 0);
        input = true;
        yield return null;
    }


    public IEnumerator GoForwardTop()
    {
        for (int i = 0; i < (180 / step); i++)
        {
            Player.RotateAround(ForwardTop.position, Center.right, step);
            ArrowCenter.position += new Vector3(0, 2f, 2f) / (180f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(0, 2, 2);
        input = true;
        yield return null;
    }

    public IEnumerator GoBackTop()
    {
        for (int i = 0; i < (180 / step); i++)
        {
            Player.RotateAround(BackTop.position, -Center.right, step);
            ArrowCenter.position += new Vector3(0, 2f, -2f) / (180f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(0, 2, -2);
        input = true;
        yield return null;
    }

    public IEnumerator GoLeftTop()
    {
        for (int i = 0; i < (180 / step); i++)
        {
            Player.RotateAround(LeftTop.position, Center.forward, step);
            ArrowCenter.position += new Vector3(-2f, 2f, 0) / (180f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(-2, 2, 0);
        input = true;
        yield return null;
    }

    public IEnumerator GoRightTop()
    {
        for (int i = 0; i < (180 / step); i++)
        {
            Player.RotateAround(RightTop.position, -Center.forward, step);
            ArrowCenter.position += new Vector3(2f, 2f, 0) / (180f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(2, 2, 0);
        input = true;
        yield return null;
    }

    public IEnumerator GoForwardDown()
    {
        for (int i = 0; i < (180 / step); i++)
        {
            Player.RotateAround(Forward.position, Center.right, step);
            ArrowCenter.position += new Vector3(0, -2f, 2f) / (180f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(0, -2, 2);
        input = true;
        yield return null;
    }

    public IEnumerator GoBackDown()
    {
        for (int i = 0; i < (180 / step); i++)
        {
            Player.RotateAround(Back.position, -Center.right, step);
            ArrowCenter.position += new Vector3(0, -2f, -2f) / (180f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(0, -2, -2);
        input = true;
        yield return null;
    }

    public IEnumerator GoLeftDown()
    {
        for (int i = 0; i < (180 / step); i++)
        {
            Player.RotateAround(Left.position, Center.forward, step);
            ArrowCenter.position += new Vector3(-2f, -2f, 0) / (180f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(-2, -2, 0);
        input = true;
        yield return null;
    }

    public IEnumerator GoRightDown()
    {
        for (int i = 0; i < (180 / step); i++)
        {
            Player.RotateAround(Right.position, -Center.forward, step);
            ArrowCenter.position += new Vector3(2f, -2f, 0) / (180f / (float)step);
            yield return new WaitForSeconds(speed);
        }

        Center.position += new Vector3(2, -2, 0);
        input = true;
        yield return null;
    }

    public void SetCurrentGround(GroundState groundState) {
        CurrentGround = groundState;
    }

    public void SetDirection(Direction dir)
    {
        Direction = dir;
    }
}
