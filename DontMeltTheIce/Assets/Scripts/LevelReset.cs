﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelReset : MonoBehaviour
{
    [SerializeField] private PlayerController PlayerController;
    [SerializeField] private FireTrigger FireTrigger;

    [SerializeField] private GameObject WinPanel;
    [SerializeField] private GameObject LosePanel;
    [SerializeField] private GameObject StartingPosition;
    
    [SerializeField] private GameObject Player;
    [SerializeField] private GameObject PlayerCenter;
    [SerializeField] private GameObject ArrowCenter;

    [SerializeField] private GameObject Arrow;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadLevel();
        }
    }

    public void ReloadLevel()
    {

        PlayerCenter.transform.position = StartingPosition.transform.position;

        Player.transform.position = StartingPosition.transform.position + new Vector3(0,0,0.01f);
        Player.transform.rotation = StartingPosition.transform.rotation;

        ArrowCenter.transform.position = StartingPosition.transform.position + new Vector3(1, 2, -1);

        Arrow.transform.localPosition = new Vector3(-0.875f, 0f, -3.5f);
        Arrow.transform.rotation = Quaternion.Euler(90, 0, 0);

        PlayerController.SetDirection(Direction.FORWARD);

        FireTrigger.SetIceMelting(false);

        WinPanel.SetActive(false);
        LosePanel.SetActive(false);
    }
}
