﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalTrigger : MonoBehaviour
{
    [SerializeField] GameObject WinPanel;
    [SerializeField] FireTrigger fireTrigger;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("I see " + other.tag);
        if(other.CompareTag("Player") && !fireTrigger.CheckIfIceMelting())
        {
            WinPanel.SetActive(true);
        }
    }
}
