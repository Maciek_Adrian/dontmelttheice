﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTrigger : MonoBehaviour
{
    [SerializeField] private GameObject LosePanel;
    private bool meltingIce = false;
    public void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<GroundState>() != null)
        {
            if(other.GetComponent<GroundState>().GetGroundType() == GroundType.ICE)
            {
                meltingIce = true;
                LosePanel.SetActive(true);
            }
        }
    }

    public bool CheckIfIceMelting()
    {
        return meltingIce;
    }

    public void SetIceMelting(bool value)
    {
        meltingIce = value;
    }

}
