﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LosePanel : MonoBehaviour
{
    [SerializeField] private GameEvent ReloadEvent;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ReloadEvent.Raise();
        }
    }
}
