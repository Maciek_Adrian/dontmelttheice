﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    int currentLevelNumber;

    private void Start()
    {
        currentLevelNumber = 0;
    }

    public void setActiveLevel(int number)
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        transform.GetChild(number).gameObject.SetActive(true);
        transform.GetChild(number).GetChild(1).GetComponent<LevelReset>().ReloadLevel();

    }

    public void ActivateNextLevel()
    {
        currentLevelNumber++;
        if (currentLevelNumber >= transform.childCount) return;

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        transform.GetChild(currentLevelNumber).gameObject.SetActive(true);
        transform.GetChild(currentLevelNumber).GetChild(1).GetComponent<LevelReset>().ReloadLevel();

    }
}
